import 'package:app_food/Providers/cart.dart';
import 'package:app_food/models/food_item.dart';
import 'package:app_food/widgets/badge.dart';
import 'package:app_food/widgets/button_caculator.dart';
import 'package:app_food/widgets/product_sliders.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';
import 'package:app_food/widgets/widget_cost.dart';
class DetailItem extends StatefulWidget {

  DetailItem({Key key}) : super(key: key);

  @override
  _DetailItemState createState() => _DetailItemState();
}

class _DetailItemState extends State<DetailItem> {
  int quantityMain = 1;

  void funtionAddQuantity(){
    setState(() {
      quantityMain +=1;
    });
  }
  void funtionRemoveQuantity(){
    if (quantityMain > 1)
    setState(() {
      quantityMain -=1;
    });
  }
  @override
  Widget build(BuildContext context) {
    final Cart cart = Provider.of<Cart>(context, listen: false);
    Size size = MediaQuery.of(context).size;
    final Food arguments =  ModalRoute.of(context).settings.arguments ;
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading:Align(
            alignment: Alignment.topLeft,
            child: IconButton(
              color: Colors.black,
            icon: Icon(LineIcons.arrow_left),
            onPressed: () {
              Navigator.pop(context);
            }), 
          ),
          actions: [ GestureDetector(
            onTap: (){Navigator.of(context).pushNamed('/my_cart');},
            child: Container(
              margin: EdgeInsets.only(right: 20, bottom: 8),
              child: Consumer<Cart>(builder: (_, cart, child) => 
                Badge(
                  child: child,
                  color: Theme.of(context).primaryColor,
                  value: cart.itemCount.toString(),
                  ),
                  child: Image.asset(
                    'assets/cart icon2.png',
                    height: 25,
                  ),
                ),
            ),
          ),
          ],
        ),
        body: ListView(
        padding: EdgeInsets.only(right: 10, left: 10, bottom: 30),
        children: <Widget>[
          
          ProductSlider(
           items:arguments.images
          ),

          SizedBox(height: 10,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
               Center(
                 child: ButtonCaculator(quantity: quantityMain, funtionAddQuantity: funtionAddQuantity, funtionRemoveQuantity: funtionRemoveQuantity,),
               ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 20, top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(arguments.name,style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 22
                      ),
                    ),
                    WidgetCost(cost: arguments.cost)
              ],),),
            SizedBox(height: 20,),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
              Text(arguments.rate, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),),
              Text('🔥 ' + arguments.calorie.toString() + " Calories", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),),
              Text(arguments.time, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),),
            ],),

            Container(
              margin: EdgeInsets.only(left: 10, top: 20),
              child: Text('Details',style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),
            ),
            Container(
              margin: EdgeInsets.only(left: 10, top: 10),
              child: Text(arguments.description, style: TextStyle(height: 1.3),),
            ),
            Container(
              margin: EdgeInsets.only(left: 10, top: 15),
              child: Text('Ingredients',style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),
            ),
            listRawMaterial(size, context, arguments),
          ],
        ),],),

        floatingActionButton:
        FloatingActionButton(
          backgroundColor: Theme.of(context).primaryColor,
          onPressed: () {
            cart.addItem(arguments.id, price:arguments.cost, food: arguments, quantity: quantityMain);
          },
          child: Container(
            child: Icon(
              Icons.add,
              size: 32,
              color: Theme.of(context).primaryColor !=  Colors.black ? Colors.black : Colors.white,
            ),
          ),
          elevation: 7,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      ),
    );
  }
}

Widget priceItem(Food arguments, int quantity){
  double price = arguments.cost * quantity;
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("Total Price",style: TextStyle(fontSize: 15),),
      SizedBox(height: 10),
      Text('\$'+ price.toStringAsFixed(2), style: TextStyle(
        fontSize: 23,
        fontWeight: FontWeight.bold
      ),)
    ],
  );
}

Widget listRawMaterial(Size size, BuildContext context, Food arguments){
   return Container(
    margin: EdgeInsets.only(top: 10),
    height: size.height * 0.12,
    width: MediaQuery.of(context).size.width,
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemBuilder: (ctx, i){
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Text(arguments.rawMaterial[i], style: TextStyle(fontSize: 40)),
        );
    },
    itemCount: arguments.rawMaterial.length,
    )
  );
}