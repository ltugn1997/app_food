import 'package:app_food/Providers/setting.dart';
import 'package:app_food/src/colors.dart';
import 'package:app_food/src/material_color_picker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingScreen extends StatefulWidget {
  SettingScreen({Key key}) : super(key: key);

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {

  void _openDialog(String title, Widget content) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(6.0),
          title: Text(title),
          content: content,
          actions: [
            FlatButton(
              child: Text('CANCEL'),
              onPressed: Navigator.of(context).pop,
            ),
            Consumer<SettingModel>(builder: (_, settingModel, child){
              return FlatButton(
              child: Text('SUBMIT'),
                onPressed: () {
                  Navigator.of(context).pop();
                  settingModel.changeColor(settingModel.tempAppColor);
                },
              );
            }),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {

    SettingModel settingModel = Provider.of<SettingModel>(context, listen: true);
    return ListTile(
      leading: Icon(Icons.settings),
      title: Text('Settings', style: TextStyle(
        fontSize: 18
      ),),
      onTap: (){
        _openDialog(
        "Full Material Color picker",
        MaterialColorPicker(
          colors: fullMaterialColors,
          selectedColor: settingModel.appColor,
          onMainColorChange: (color){
           settingModel.changeTempColor(color);
          },
          onColorChange:(color){
            settingModel.changeTempColor(color);
          },
          ),
        );
      }
    );
  }
}
