import 'dart:ui';
import 'package:app_food/Providers/auth.dart';
import 'package:app_food/screens/signup_scren.dart';
import 'package:app_food/widgets/customtextfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController gmailController =  TextEditingController();
  TextEditingController passWordController = TextEditingController();

  @override
    void initState() {
      gmailController.text = 'demo@gmail.com';
      passWordController.text = 'demo123';
      super.initState();
    }

  @override
  Widget build(BuildContext context) {
    final AuthUsers users = Provider.of<AuthUsers>(context, listen: false);

    void _showCupertinoDialog() {
        showDialog(
          context: context,
          builder: (_) => new CupertinoAlertDialog(
            title: new Text("Wrong Information"),
            content: new Text("UserName or Password incorrect. Please Check again"),
            actions: <Widget>[
              Container(
                child: FlatButton(
                  child: Text('Try Again'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ),
            ],
          )
        );
      }
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Form(
          key: _formKey,
          child: Container(
            decoration: BoxDecoration(
              // image: DecorationImage(
              //     image: AssetImage('assets/pizza.png'), fit: BoxFit.cover),
              gradient: LinearGradient(
                  colors: [Colors.white, Theme.of(context).primaryColor],
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter),
            ),
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 150,
                  ),
                  Row(
                    children: <Widget>[
                      SizedBox(
                        width: 40,
                      ),
                      Text(
                        'Welcome Back',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 35),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      SizedBox(
                        width: 40,
                      ),
                      Text(
                        'Sign in with your account',
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 65,
                  ),
                  CustomTextField(
                    gmailController,
                    issecured: false,
                    hint: '    Email/Phone',
                    type: 'Gmail',
                    textInputType: TextInputType.emailAddress,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomTextField(
                    passWordController,
                    hint: '   Password',
                    issecured: true,
                    //type: 'Password',
                    textInputType: TextInputType.visiblePassword
                  ),
                  // SizedBox(
                  //   height: 20,
                  // ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.end,
                  //   children: <Widget>[
                  //     GestureDetector(
                  //       onTap: () {},
                  //       child: Container(
                  //         child: Text(
                  //           'Forgot Password?',
                  //           style: TextStyle(
                  //               color: Colors.black, fontWeight: FontWeight.w700),
                  //         ),
                  //       ),
                  //     ),
                  //     SizedBox(
                  //       width: 40,
                  //     )
                  //   ],
                  // ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 25, right: 25),
                    child: ButtonTheme(
                        buttonColor: Colors.black,
                        minWidth: MediaQuery.of(context).size.width,
                        height: 55,
                        child: RaisedButton(
                          onPressed: () {
                            bool isValidType = _formKey.currentState.validate();
                            String isValid = checkLogin(users.items, gmailController.text, passWordController.text);
                            if(isValid.isEmpty == false && isValidType){
                              users.getCurrentUser(isValid);
                              Navigator.pushNamed(context,'/');
                              return;
                            }
                            return _showCupertinoDialog();
                          },
                          child: Text(
                            'Log in',
                            style: TextStyle(color: Colors.grey, fontSize: 22),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25)),
                        )),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Don't Have an Accout ?",
                        style: TextStyle(color: Colors.black),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (ctx) => SignUpScreen()));
                        },
                        child: Text(
                          'Sign up',
                          style: TextStyle(
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.w500,
                              color: Colors.black),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

String checkLogin(List<AuthUser> users, String userName, String passWord){
  List checks = users.where((AuthUser authUser) => authUser.gmail == userName && authUser.passWord == passWord).toList();
  if (checks.isEmpty){
    return '';
  }
    return checks[0].id;
}