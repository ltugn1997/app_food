import 'package:app_food/Providers/auth.dart';
import 'package:app_food/screens/setting_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainDrawer extends StatelessWidget {
  MainDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthUsers authUser = Provider.of<AuthUsers>(context, listen: true);
    return Drawer(
      child: Column(children: <Widget>[
        Container(
          width: double.infinity,
          padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Colors.white24, Theme.of(context).primaryColor],
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter),
          ),
          child: Center(
            child: Column(
              children: [
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: ExactAssetImage('assets/avata.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Text(authUser.selectedUser.userName,
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold
                  ),
                ),
                Text(authUser.selectedUser.gmail,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.w400
                  ),
                ),
              ],
            ),
          ),
        ),
        ListTile(
          leading: Icon(Icons.person),
          title: Text('Profile', style: TextStyle(
            fontSize: 18
          ),),
          onTap: (){
            Navigator.of(context).pushNamed('/profile_screen');
          }
        ),
        ListTile(
          leading: Icon(Icons.phone),
          title: Text('Connect', style: TextStyle(
            fontSize: 18
          ),),
          onTap: (){
            Navigator.of(context).pushNamed('/connect_screen');
          }
        ),
        SettingScreen(),
        ListTile(
          leading: Icon(Icons.arrow_back),
          title: Text('Logout', style: TextStyle(
            fontSize: 18
          ),),
          onTap: (){
            Navigator.of(context).pushReplacementNamed('/login_screen');
          }
        ),

      ],)
    );
  }
}