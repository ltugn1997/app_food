import 'package:app_food/screens/my_cart.dart';
import 'package:app_food/screens/orders_screen.dart';
import 'package:app_food/screens/welcome.dart';
import 'package:app_food/widgets/bottom_navigation_bar.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int page = 0;
  PageController pageController = PageController(
    initialPage: 0
  );
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        extendBody: true,
        bottomNavigationBar: generateBottomNavigationBar(context, 0),
        body: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: pageController,
          onPageChanged: (index){
            setState(() {
              page = index;
            });
          },
          children: [
            WelcomeScreen(),
            MyCart(),
            OrdersScreen(),
          ],
        ),
      )
    );
  }
}
