import 'package:app_food/Providers/auth.dart';
import 'package:app_food/widgets/customtextfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SignUpScreen extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController =  TextEditingController();
  TextEditingController gmailController = TextEditingController();
  TextEditingController phoneController =  TextEditingController();
  TextEditingController passWordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final AuthUsers authUser = Provider.of<AuthUsers>(context, listen: false);
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Form(
          key: _formKey,
          child: Container(
            decoration: BoxDecoration(
              // image: DecorationImage(
              //     image: AssetImage('images/1.png'), fit: BoxFit.cover),
              gradient: LinearGradient(
                  colors: [Colors.white,Theme.of(context).primaryColor],
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter),
            ),
            child: Center(
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: 50,
                  ),
                  Row(
                    children: <Widget>[
                      SizedBox(
                        width: 40,
                      ),
                      Text(
                        'Create Account',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 35),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  CustomTextField(
                    nameController,
                    hint: '    enter your name',
                    issecured: false,
                    textInputType: TextInputType.name
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  CustomTextField(
                    gmailController,
                    hint: '    enter your Email',
                    issecured: false,
                    type: 'Gmail',
                    textInputType: TextInputType.emailAddress
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  CustomTextField(
                    phoneController,
                    hint: '    enter your Phone',
                    issecured: false,
                    textInputType: TextInputType.number
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  CustomTextField(
                    passWordController,
                    hint: '    enter your Password',
                    issecured: false,
                    //type: 'Password',
                    textInputType: TextInputType.visiblePassword
                  ),
                  SizedBox(
                    height: 25,
                  ),  Padding(
                    padding: const EdgeInsets.only(left: 25, right: 25),
                    child: ButtonTheme(
                        buttonColor: Colors.black,
                        minWidth: MediaQuery.of(context).size.width,
                        height: 55,
                        child: RaisedButton(
                          onPressed: () {
                            bool isValidType = _formKey.currentState.validate();
                            if(isValidType){
                              authUser.addUser(gmailController.text, passWordController.text, nameController.text, phoneController.text );
                              Navigator.of(context).pushNamed('/login_screen');
                            }
                           },
                          child: Text(
                            'Create',
                            style: TextStyle(color: Colors.grey, fontSize: 22),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25)),
                        )),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
