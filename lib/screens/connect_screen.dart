import 'package:flutter/material.dart';

class ConnectScreen extends StatefulWidget {
  @override
  _ConnectScreenState createState() => _ConnectScreenState();
}

class _ConnectScreenState extends State<ConnectScreen> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(title: Text('Connect'),),
      body: Container(
        padding: EdgeInsets.only(top: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            rowInfo(Icons.person, 'name', 'Le Cam Tung'),
            rowInfo(Icons.phone, 'Phone', '+84961325198'),
          ]
        )
      )
    );
  }
}

Widget rowInfo(IconData icon, String title, String content){
  return Container(
    padding: EdgeInsets.only(top: 15, left: 20),
    child: Row(
      children: [
      Icon(icon),
      SizedBox(width: 10,),
      Text(title + ":", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
      SizedBox(width: 10),
      Text(content, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),)
    ],)
  );

}




