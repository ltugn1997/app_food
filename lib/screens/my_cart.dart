import 'package:app_food/Providers/cart.dart';
import 'package:app_food/Providers/order.dart';
import 'package:app_food/models/food_item.dart';
import 'package:app_food/widgets/bottom_navigation_bar.dart';
import 'package:app_food/widgets/my_order_cart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:app_food/widgets/widget_cost.dart';

class MyCart extends StatefulWidget {
  MyCart({Key key}) : super(key: key);

  @override
  _MyCartState createState() => _MyCartState();
}

class _MyCartState extends State<MyCart> {
  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<Cart>(context);
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      bottomNavigationBar: generateBottomNavigationBar(context, 1),
      appBar: AppBar(
         automaticallyImplyLeading: false,
        centerTitle: false,
        titleSpacing: 0.0,
        backgroundColor: Colors.white,
        title:Container(
          margin: EdgeInsets.only(left: 20),
          child: Text('Your Cart', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black),),
        ),
      ),
      body: cart.items.length == 0 ?
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
          GestureDetector(
            onTap: (){Navigator.of(context).pushNamed('/');},
            child: Container(
            child: Center(
              child: Lottie.asset('assets/empty_cart.json', height: 150)
            ),
            ),
          ),
          Container(child: Text("Your Cart Is Currently Empty!", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold,),),)
        ],)
        :
      Column(children: [
        Expanded(
          child: ListView.builder(
            itemBuilder: (ctx, i) {
              CartItem dataRecommendItem = cart.items.values.toList()[i];
              Food recommendItem = dataRecommendItem.food;
              return Column(children: [
                  Hero(
                    tag: (recommendItem.images[0]),
                    child: Dismissible(
                      key: ValueKey(dataRecommendItem.id),
                      background: Container(
                        color: Colors.red,
                        child: Icon(Icons.delete, color: Colors.white, size: 40,),
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.only(right: 20),
                        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
                      ),
                      direction: DismissDirection.endToStart,
                      onDismissed: (direction){
                        cart.removeItem(recommendItem.id);
                      },
                      child: MyCartCard(
                      quantity: dataRecommendItem.quantity,
                      imgURl: recommendItem.images[0],
                      cost: recommendItem.cost,
                      name: recommendItem.name,
                      entrance: recommendItem.entrance,
                      id: recommendItem.id,
                      ),
                    ),
                ),
                 ],);
            },
            itemCount: cart.items.length,
          ),
        ),
        Container(
            color: Colors.grey.withOpacity(0.2),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Row(children: [
                Text('Total: ', style: TextStyle(fontSize: 25),),
                WidgetCost(cost:cart.totalAmount),
              ],),
            ),
            GestureDetector(
              onTap: (){
                Provider.of<Orders>(context, listen: false).addOrder(
                  cart.items.values.toList(),
                  cart.totalAmount,
                );
                showDialog(
                  context: context,
                  builder: (_) => CupertinoAlertDialog(
                    title: Text("Transaction Success"),
                  )
                );
                cart.clear();
              },
              child: Container(
                width: 70,
                child: Image.asset('assets/atm.png',fit:  BoxFit.contain,),
              ),
            ),
          ],)
        ),
      ],),
    );
  }
}