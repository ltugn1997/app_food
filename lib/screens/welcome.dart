import 'dart:ui';
import 'package:app_food/Providers/food_items.dart';
import 'package:app_food/models/food_item.dart';
import 'package:app_food/screens/main_drawer.dart';
import 'package:app_food/units/constant.dart';
import 'package:app_food/widgets/categories_card.dart';
import 'package:app_food/widgets/recommend_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WelcomeScreen extends StatefulWidget {
  WelcomeScreen({Key key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    final foodsData = Provider.of<Foods>(context);
    final foods = foodsData.items;
    final Size size = MediaQuery.of(context).size;
    return SafeArea(
        child:  Scaffold(
          drawer: Drawer(
            child:MainDrawer(),
            ),
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            leading:Builder(
              builder: (context) => IconButton(
                icon: Icon(Icons.menu, size: 30, color: Theme.of(context).primaryColor,),
                onPressed: () => Scaffold.of(context).openDrawer(),
              ),
            ),
            title:
              Text('Food Delivery',
              style: TextStyle(
                color: Colors.black87, fontSize: 24, fontWeight: FontWeight.bold),),
            actions: [
            Stack(
              overflow: Overflow.clip,
              children: [
              Container(
                width: 50,
                height: 50,
                margin: EdgeInsets.only(top: 8, right: 15),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  shape: BoxShape.circle,
                ),
              ),
             Positioned(
               left: 0,
               right: 15,
               top: 8,
               bottom: 0,
               child: Container(
                   width: 80,
                   height: 50,
                   decoration: BoxDecoration(
                     shape: BoxShape.circle,
                     image: DecorationImage(
                       fit: BoxFit.fitHeight,
                       image: ExactAssetImage('assets/avata.png'),
                     ),
                   ),
                 ),
             ),
            ],)
            ],
          ),
          body: ListView(
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            children: [
            welcomeHeader(context),
            Categories(),
            listItemsRecommend(size, foods),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (ctx, i) {
                Food recommendItem = foods[i];
                String image = recommendItem.images[0];
                return InkWell(
                  onTap: (){ Navigator.pushNamed(context, '/detail_item', arguments: recommendItem);},
                  child: RecommendCard(
                  imgURl: image,
                  cost: recommendItem.cost,
                  name: recommendItem.name,
                  entrance: recommendItem.entrance,
                  ),
                );
              },
              itemCount: foods.length,
            ),
          ],
        ),
      ),
    );
  }
}
class DataSearch extends SearchDelegate<String>{
  final allfoods = [...Contants.dishItems, ...Contants.pizzaItems, ...Contants.pastas];

  final recommandFood = Contants.dishItems;

  @override
  List<Widget> buildActions(BuildContext context) {
      return [
        IconButton(icon: Icon(Icons.clear), onPressed: (){
          query = "";
        },)
      ];
    }

    @override
    Widget buildLeading(BuildContext context) {
      return IconButton(
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ),
        onPressed: (){
          close(context, null);
        },
      );
    }

    @override
    Widget buildResults(BuildContext context) {
      return buildSuggestions(context);
    }

    @override
    Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty?recommandFood:allfoods.where((food)=> food.name.toLowerCase().contains(query.toLowerCase())).toList();

    return ListView.builder(itemBuilder: (context, index)=>
    Container(
      margin: EdgeInsets.symmetric(vertical: 2),
      child: ListTile(
        onTap: (){
          Navigator.pushNamed(context, '/detail_item', arguments: suggestionList[index]);
        },
        leading: Image.asset(suggestionList[index].images[0]),
        title: Text(suggestionList[index].name,style: TextStyle(color: Colors.grey )),
      ),
    ),
    itemCount:suggestionList.length ,
    );
  }
}

Widget welcomeHeader(BuildContext context){
  return Padding(
    padding: const EdgeInsets.only(left: 32.0, top: 12),
    child: Row(children: [
      Text('Let\'s eat \nQuality food', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
      Container(
        padding: EdgeInsets.only(top: 0),
        child: IconButton(icon: Icon(Icons.search, color: Colors.black,size: 50,), onPressed: (){
          showSearch(context: context, delegate: DataSearch());
        })
      )
    ],)
  );
}


Widget listItemsRecommend(Size size, List<Food> foods){
  return
    Container(
      height: size.height * 0.34,
      child: ListView.builder(
        itemCount: foods.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, i) {
          Food recommendItem = foods[i];
          String image = recommendItem.images[0];
          return GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/detail_item', arguments: recommendItem);
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: 170,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                    BorderRadius.all(Radius.circular(15))),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        image,
                        height: 100,
                        width: 200,
                      ),
                      Text(
                        recommendItem.name,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w700),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        '🔥' + recommendItem.calorie.toString() +' Calories',
                        style: TextStyle(
                            fontSize: 20, color: Colors.red),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      RichText(
                        text: TextSpan(children: [
                        TextSpan(
                          text: '\$',
                          style: TextStyle(
                              fontSize: 24,
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: recommendItem.cost.toStringAsFixed(2),
                          style: TextStyle(
                            fontSize: 24,
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }
),);
}