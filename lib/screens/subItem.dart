import 'package:app_food/Providers/food_type.dart';
import 'package:app_food/models/food_item.dart';
import 'package:app_food/units/constant.dart';
import 'package:app_food/widgets/bottom_navigation_bar.dart';
import 'package:app_food/widgets/recommend_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SubItem extends StatefulWidget {
  SubItem({Key key}) : super(key: key);

  @override
  _SubItemState createState() => _SubItemState();
}

class _SubItemState extends State<SubItem> {
  @override
  Widget build(BuildContext context) {
    final String arguments =  ModalRoute.of(context).settings.arguments ;
    final Foodtypes foodtypes = Provider.of<Foodtypes>(context, listen: false);
    List<Food> foods ;
    if (arguments == 'Pasta'){
      foods = Contants.pastas;
    }
    if (arguments != 'Pasta'){
      foods = foodtypes.items[0].foods; 
    }
    return SafeArea( 
        child: Scaffold(
          bottomNavigationBar: generateBottomNavigationBar(context, 0),
          body:Container(
            margin: EdgeInsets.only(left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [   
                Container(
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blueAccent,
                          spreadRadius: 2,
                          blurRadius: 2,
                          offset: Offset(0, 0),
                        )
                      ],
                      color: Colors.white,
                      border: Border.all(
                        color: Colors.blueAccent,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                    ),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Text(arguments + '😋', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25, fontStyle: FontStyle.italic),), 
                  )
                ),     
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 10, right: 10),
                    child: ListView.builder(
                      itemBuilder: (ctx, i) {
                        Food recommendItem = foods[i];
                        String image = recommendItem.images[0];
                        return InkWell(
                          onTap: (){ Navigator.pushNamed(context, '/detail_item', arguments: recommendItem);},
                          child: RecommendCard(
                          imgURl: image,
                          cost: recommendItem.cost,
                          name: recommendItem.name,
                          entrance: recommendItem.entrance,
                          ),         
                        );
                      },
                    itemCount: foods.length,
                  )
                )
              )
            ],    
          ),
        ),
      ),  
    );
  }
}