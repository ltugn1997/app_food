import 'package:app_food/Providers/order.dart';
import 'package:app_food/widgets/bottom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app_food/widgets/order_item.dart';
import 'package:lottie/lottie.dart';

class OrdersScreen extends StatelessWidget {
  static const routeName = '/orders';

  @override
  Widget build(BuildContext context) {
    final orderData = Provider.of<Orders>(context);
    return Scaffold(
      bottomNavigationBar: generateBottomNavigationBar(context, 2),
      appBar:AppBar(
        automaticallyImplyLeading: false,
        centerTitle: false,
        titleSpacing: 0.0,
        backgroundColor: Colors.white,
        title:Container(margin: EdgeInsets.only(left: 20),
          child: Text('Your Order', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black),),
        ),
      ),
      body: (orderData.orders.length == 0)?
        Container(
          margin: EdgeInsets.only(bottom: 100),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
            Container(
              child: Center(
                child: Text('Order Now', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),),
              ),
            ),
            Lottie.asset('assets/point_down.json', height: 150),
            GestureDetector(
              onTap: (){Navigator.of(context).pushNamed('/');},
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Theme.of(context).primaryColor, width: 3)
                ),
                child: Text("Click Me !"),)
            ),
          ],)
        ):
        ListView.builder(
          itemCount: orderData.orders.length,
          itemBuilder: (ctx, i) => OrderItemWiget(orderData.orders[i]
        ),
      ),
    );
  }
}
