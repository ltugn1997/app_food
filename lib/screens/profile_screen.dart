import 'package:app_food/Providers/auth.dart';
import 'package:app_food/widgets/customtextfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final AuthUsers authUser = Provider.of<AuthUsers>(context, listen: false);
      nameController.text = authUser.selectedUser.userName;
      gmailController.text = authUser.selectedUser.gmail;
      phoneController.text = authUser.selectedUser.phone;
      passWordController.text = authUser.selectedUser.passWord;
    });
  }
  TextEditingController nameController =  TextEditingController();
  TextEditingController gmailController = TextEditingController();
  TextEditingController phoneController =  TextEditingController();
  TextEditingController passWordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final AuthUsers authUser = Provider.of<AuthUsers>(context, listen: false);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Colors.white,Theme.of(context).primaryColor],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter),
        ),
        child: Center(
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 50,
              ),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 40,
                  ),
                  Text(
                    'Edit Account',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 35),
                  ),
                ],
              ),
              SizedBox(
                height: 50,
              ),
              CustomTextField(
                nameController,
                hint: '    enter your name',
                issecured: false,
                textInputType: TextInputType.name,
              ),
              SizedBox(
                height: 15,
              ),
              CustomTextField(
                gmailController,
                hint: '    enter your Email',
                issecured: false,
                type: 'Gmail',
                textInputType: TextInputType.emailAddress,
              ),
              SizedBox(
                height: 15,
              ),
              CustomTextField(
                phoneController,
                hint: '    enter your Phone',
                issecured: false,
                textInputType: TextInputType.number,
              ),
              SizedBox(
                height: 15,
              ),
              CustomTextField(
                passWordController,
                hint: '    enter your Password',
                issecured: false,
                //type: 'Password',
                textInputType: TextInputType.visiblePassword,
              ),
              SizedBox(
                height: 25,
              ),  Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: ButtonTheme(
                    buttonColor: Colors.black,
                    minWidth: MediaQuery.of(context).size.width,
                    height: 55,
                    child: RaisedButton(
                      onPressed: () {
                        authUser.editUser(authUser.selectedUser.id, gmailController.text, passWordController.text, nameController.text, phoneController.text );
                        Navigator.of(context).pushNamed('/');},
                      child: Text(
                        'Edit',
                        style: TextStyle(color: Colors.grey, fontSize: 22),
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25)),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}





