import 'package:app_food/Providers/auth.dart';
import 'package:app_food/Providers/food_type.dart';
import 'package:app_food/models/food_item.dart';
import 'package:flutter/material.dart';

class Contants{
  static OutlineInputBorder border = OutlineInputBorder(
    borderRadius: BorderRadius.circular(30),
    borderSide: BorderSide(color: Colors.transparent),
  );

  static List<AuthUser> users = [
    AuthUser(id:'1', userName: 'Demo', passWord: 'demo123', gmail: 'demo@gmail.com', phone: '0961325198')
    ];

  static List<Food> dishItems = [
    Food(id:'1',
      images:["assets/salad_1.png", "assets/salad_4.png", "assets/salad_5.png",],
      name: 'Chile Rice', cost: 10, calorie: 500, squirrels: 60, fats: 33, entrance: 400, description: 'A salad is a dish consisting of pieces of food in a mixture, with at least one raw ingredient. It is often dressed, and is typically served at room temperature or chilled, though some (such as south German potato salad, or chicken salad) can be served warm.',
      rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
      rate: '4.5 ⭐',
      time: '⏱️ 30-40 min',),

    Food(id:'2',
      images:["assets/salad_5.png", "assets/salad_2.png", "assets/salad_3.png",],
      name: 'Beef Steak', cost: 20, calorie: 450, squirrels: 60, fats: 35, entrance: 450, description: 'A salad is a dish consisting of pieces of food in a mixture, with at least one raw ingredient. It is often dressed, and is typically served at room temperature or chilled, though some (such as south German potato salad, or chicken salad) can be served warm.',
      rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
      rate: '4.5 ⭐',
      time: '⏱️ 25-40 min',),

    Food(id:'3',
      images:["assets/salad_4.png", "assets/salad_5.png", "assets/salad_3.png",],
      name: 'Fish Fry', cost: 15, calorie: 350, squirrels: 60, fats: 23, entrance: 250, description: 'A salad is a dish consisting of pieces of food in a mixture, with at least one raw ingredient. It is often dressed, and is typically served at room temperature or chilled, though some (such as south German potato salad, or chicken salad) can be served warm.',
      rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
      rate: '4.5 ⭐',
      time: '⏱️ 25-40 min',),

    Food(id:'4',
      images:["assets/salad_3.png", "assets/salad_5.png", "assets/salad_2.png",],
      name: 'Pork Chops', cost: 23, calorie: 450, squirrels: 60, fats: 43, entrance: 350, description: 'A salad is a dish consisting of pieces of food in a mixture, with at least one raw ingredient. It is often dressed, and is typically served at room temperature or chilled, though some (such as south German potato salad, or chicken salad) can be served warm.',
      rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
      rate: '4.5 ⭐',
      time: '⏱️ 25-40 min',),

    Food(id:'5',
      images:["assets/salad_1.png", "assets/salad_4.png", "assets/salad_5.png",],
      name: 'Special Chile Rice', cost: 10, calorie: 500, squirrels: 60, fats: 33, entrance: 400, description: 'A salad is a dish consisting of pieces of food in a mixture, with at least one raw ingredient. It is often dressed, and is typically served at room temperature or chilled, though some (such as south German potato salad, or chicken salad) can be served warm.',
      rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
      rate: '4.5 ⭐',
      time: '⏱️ 25-40 min',),

    Food(id:'6',
      images:["assets/salad_5.png", "assets/salad_2.png", "assets/salad_3.png",],
      name: 'Special Beef Steak', cost: 20, calorie: 450, squirrels: 60, fats: 35, entrance: 450, description: 'A salad is a dish consisting of pieces of food in a mixture, with at least one raw ingredient. It is often dressed, and is typically served at room temperature or chilled, though some (such as south German potato salad, or chicken salad) can be served warm.',
      rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
      rate: '4.5 ⭐',
      time: '⏱️ 25-40 min',),

    Food(id:'7',
      images:["assets/salad_4.png", "assets/salad_5.png", "assets/salad_3.png",],
      name: 'Special Fish Fry', cost: 15, calorie: 350, squirrels: 60, fats: 23, entrance: 250, description: 'A salad is a dish consisting of pieces of food in a mixture, with at least one raw ingredient. It is often dressed, and is typically served at room temperature or chilled, though some (such as south German potato salad, or chicken salad) can be served warm.',
      rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
      rate: '4.5 ⭐',
      time: '⏱️ 25-40 min',),
    ];

  static List<Food> pizzaItems = [
    Food(id:'8',
    images:['assets/americana_pizza.png', 'assets/classic_pizaa.png', 'assets/mexicanPizza_pizza.png', 'assets/veg_pizza.png',],
    name: 'Americana Pizza',
    cost: 23, calorie: 500, squirrels: 60, fats: 33, entrance: 400,
    description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
    rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
    rate: '4.5 ⭐',
    time: '⏱️ 25-40 min',),

    Food(id:'9',
    images:['assets/classic_pizaa.png', 'assets/americana_pizza.png', 'assets/mexicanPizza_pizza.png', 'assets/veg_pizza.png',],
    name: 'Classic Pizaa',
    cost: 18, calorie: 450, squirrels: 60, fats: 35, entrance: 450,
    description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
    rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
    rate: '4.5 ⭐',
    time: '⏱️ 25-40 min',),

    Food(id:'10',
    images:['assets/mexicanPizza_pizza.png', 'assets/americana_pizza.png', 'assets/classic_pizaa.png','assets/veg_pizza.png',],
    name: 'Mexican Pizza',
    cost: 15, calorie: 350, squirrels: 60, fats: 23, entrance: 250,
    description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
    rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
    rate: '4.5 ⭐',
    time: '⏱️ 25-40 min',),

    Food(id:'11',
    images:['assets/veg_pizza.png', 'assets/americana_pizza.png', 'assets/classic_pizaa.png', 'assets/mexicanPizza_pizza.png'],
    name: 'Vegestable Pizza',
    cost: 10, calorie: 450, squirrels: 60, fats: 43, entrance: 350,
    description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
    rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
    rate: '4.5 ⭐',
    time: '⏱️ 25-40 min',)
  ];

  static List<Foodtype> carts =[
    Foodtype( image: '🍣', title:'Japanese',
      color:Colors.blueAccent,
      colorText:Colors.blue,
      profileImage:'assets/plate1.png',
      foods:Contants.dishItems,
      type:['Sukiyaki', 'Tempura', 'Sukiyaki', 'Ramen', 'Kaiseki Ryori'],
    ),
     Foodtype( image: '🍕', title:'Italian',
      color: Colors.orangeAccent,
      colorText:Colors.orange,
      profileImage:'assets/mexicanPizza_pizza.png',
      foods:Contants.pizzaItems,
      type:['Pizza', 'Pasta', 'Basilico', 'Panzenella', 'Bruschetta'],
    ),
     Foodtype( image: '🦐', title:'Chinese',
      color:Colors.redAccent,
      colorText:Colors.red,
      profileImage: 'assets/plate3.png',
      foods:Contants.dishItems,
      type:['Fried Noodles', 'Dim Sum', 'Peking Duck', 'Wonton Soup', 'Chinese Tea Set'],
    ),
     Foodtype( image: '🍔', title:'America',
      color:Colors.greenAccent,
      colorText:Colors.green,
      profileImage:'assets/plate4.png',
      foods:Contants.dishItems,
      type:['Pasta', 'Cobb Salad', 'Pot Roast', 'Fajitas',  'Banana Split'],
    ),
  ];


  static List<Food> pastas = [
    Food(id:'12',
    images:['assets/pasta/new_pasta1.png', 'assets/pasta/new_pasta2.png', 'assets/pasta/new_pasta3.png', 'assets/pasta/new_pasta4.png',],
    name: 'Americana Pasta',
    cost: 23, calorie: 500, squirrels: 60, fats: 33, entrance: 400,
    description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
    rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
    rate: '4.5 ⭐',
    time: '⏱️ 25-40 min',),

    Food(id:'13',
    images:['assets/pasta/new_pasta2.png', 'assets/pasta/new_pasta3.png', 'assets/pasta/new_pasta4.png', 'assets/pasta/new_pasta1.png',],
    name: 'Classic Pasta',
    cost: 18, calorie: 450, squirrels: 60, fats: 35, entrance: 450,
    description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
    rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
    rate: '4.5 ⭐',
    time: '⏱️ 25-40 min',),

    Food(id:'14',
    images:['assets/pasta/new_pasta3.png', 'assets/pasta/new_pasta4.png', 'assets/pasta/new_pasta1.png', 'assets/pasta/new_pasta2.png',],
    name: 'Mexican Pasta',
    cost: 15, calorie: 350, squirrels: 60, fats: 23, entrance: 250,
    description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
    rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
    rate: '4.5 ⭐',
    time: '⏱️ 25-40 min',),

    Food(id:'15',
    images:['assets/pasta/new_pasta5.png', 'assets/pasta/new_pasta4.png', 'assets/pasta/new_pasta1.png', 'assets/pasta/new_pasta2.png',],
    name: 'Vegestable Pasta',
    cost: 10, calorie: 450, squirrels: 60, fats: 43, entrance: 350,
    description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
    rawMaterial: ['🥩', '🍅', '🥕', '🍆', '🥒'],
    rate: '4.5 ⭐',
    time: '⏱️ 25-40 min',)
  ];
}

