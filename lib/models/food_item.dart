import 'package:flutter/material.dart';

class Food {
  final String id;
  final List<String> images;
  final String name;
  final double cost;
  final int calorie;
  final double squirrels;
  final double fats;
  final double entrance;
  final String description;
  final List<String> rawMaterial;
  final String rate;
  final String time;

  Food({
    @required this.id,
    @required this.images,
    @required this.name,
    @required this.cost,
    @required this.calorie,
    @required this.squirrels,
    @required this.fats,
    @required this.entrance,
    @required this.description,
    @required this.rawMaterial,
    @required this.rate,
    @required this.time,
  });
}