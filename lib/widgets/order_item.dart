import 'dart:math';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:app_food/Providers/order.dart';

class OrderItemWiget extends StatefulWidget {
  final OrderItem order;

  OrderItemWiget(this.order);

  @override
  _OrderItemWigetState createState() => _OrderItemWigetState();
}

class _OrderItemWigetState extends State<OrderItemWiget> {
  var _expanded = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Card(
      margin: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
            Container(
              padding: EdgeInsets.only(left: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                Text('\$${widget.order.amount}',style: TextStyle(fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, fontSize: 20),),
                Text(DateFormat('dd/MM/yyyy hh:mm').format(widget.order.dateTime),
              ),],),
            ),
             IconButton(
              icon: Icon(_expanded ? Icons.expand_less : Icons.expand_more),
              onPressed: () {
                setState(() {
                  _expanded = !_expanded;
                });
              }
             ),
          ],),
          if (_expanded)
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
              height: min(widget.order.products.length * size.height * 0.11 + 15, 600),
              child: ListView(
                children: widget.order.products
                    .map(
                      (prod) => Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(children: [
                            Container(
                              margin: EdgeInsets.all(2),
                              height: size.height * 0.11,
                              width: size.width * 0.18,
                              child: Image.asset(prod.food.images[0],fit: BoxFit.contain,),
                            ),
                            SizedBox(width: 20,),
                            Text(
                              prod.food.name,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],),
                          Text(
                            '${prod.quantity}x \$${prod.price}',
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.grey,
                            ),
                          )
                        ],
                      ),
                    )
                    .toList(),
              ),
            )
        ],
      ),
    );
  }
}
