import 'package:flutter/material.dart';
class RecommendCard extends StatelessWidget {
  final String imgURl ;
  final double cost;
  final String name;
  final double entrance;
  const RecommendCard({Key key, this.imgURl, this.cost, this.name, this.entrance}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      width: size.width,
      height: size.height * 0.2,     
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
            top: 22,
            left: 20,
            child: Container(
              width: size.width * 0.85,
              height: size.height * 0.16, 
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.2),
                borderRadius: BorderRadius.all(Radius.circular(50)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: size.width * 0.36,
                  ),
                  Expanded(child: Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(name,style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold
                        ),),
                        SizedBox(height: 8),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                          DefaultTextStyle(style: Theme.of(context).textTheme.bodyText1, child:Text('Entrance $entrance' + 'g', style: TextStyle(color: Colors.grey[600]),),),                             
                          Icon(Icons.add_circle)
                        ],),
                        SizedBox(height: 8,),
                          Text('\$'+cost.toStringAsFixed(2), style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold
                        ),),
                      ],
                    ),
                  ))
                ],
              )
            ),
          ),
          Positioned(
            top: 10,
            left: 10,
            child: 
           Container(
              width: size.width * 0.35,
              height: size.height * 0.2,
              decoration: BoxDecoration(
                boxShadow: [BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 10
                )],
                shape: BoxShape.circle,
              ),
              child: Image.asset(imgURl),
            ),
          )
        ],
      ),
    );
  }
}