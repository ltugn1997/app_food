import 'package:flutter/material.dart';

class ButtonCaculator extends StatefulWidget {
  IconData icon;
  Color colorIcon;
  Color backGroundIcon;
  int quantity;
  Function funtionAddQuantity;
  Function funtionRemoveQuantity;
  ButtonCaculator({Key key, this.icon, this.colorIcon, this.backGroundIcon, this.quantity, this.funtionAddQuantity, this.funtionRemoveQuantity}) : super(key: key);

  @override
  _ButtonCaculatorState createState() => _ButtonCaculatorState();
}

class _ButtonCaculatorState extends State<ButtonCaculator> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double hight = size.height ;
    double width = size.width ;
    return
    Container(
      height: hight * 0.045,
      width:  width * 0.3,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                widget.funtionRemoveQuantity();
              },
              child:Icon(
                Icons.remove,
                size: 28,
                color: Theme.of(context).primaryColor !=  Colors.black ? Colors.black : Colors.white
              ),  
            ),
            Center(
              child: Text(widget.quantity.toString(),
                style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 24,
                color: Theme.of(context).primaryColor !=  Colors.black ? Colors.black : Colors.white
              ),),
            ),
            GestureDetector(
              onTap: () {
                widget.funtionAddQuantity();   
              },
              child:Icon(
                Icons.add,
                size: 28,
                color: Theme.of(context).primaryColor !=  Colors.black ? Colors.black : Colors.white
              ),
            )
          ],
        ),
      ),
    );
  }
}
