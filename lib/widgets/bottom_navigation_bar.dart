import 'package:app_food/Providers/cart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

const navigationBarRoutes = ['/', '/my_cart', '/screen_order'];

tabItems (context) => [
  BottomNavigationBarItem(
    icon: Icon(
      Icons.restaurant,
      color: Colors.grey,
      size: 30,
    ),
    activeIcon:Icon(
      Icons.restaurant,
      color: Theme.of(context).primaryColor,
      size: 30,
    ),
    title:Container(
      height: 0.0,
    )
  ),
  BottomNavigationBarItem(
    icon: Consumer<Cart>(builder: (_, cart, child) => 
      Container(
        height: 40,
        width: 70,
        decoration: BoxDecoration(
        border: Border.all(
          color: Colors.transparent, width: 4),
          color: Colors.transparent,
          shape: BoxShape.circle),
        child: Stack(
          overflow: Overflow.visible,
          children: [
            Positioned(
              bottom: 5,
              left: 2,
              child: FloatingActionButton(
                backgroundColor: Theme.of(context).primaryColor,
                onPressed: () {
                  Navigator.pushReplacementNamed(context, navigationBarRoutes[1]);
                },
                child: Icon(
                  Icons.shopping_cart,
                  color: Theme.of(context).primaryColor !=  Colors.black ? Colors.black : Colors.white,
                ),
                elevation: 5,
              ),
            ),
            Positioned(
              bottom: 34,
              left: 37,
              child: Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white, width: 4),
                    color: Theme.of(context).primaryColor,
                    shape: BoxShape.circle),
                child: Consumer<Cart>(builder: (_, cart, child) => 
                  Center(
                    child: Text(cart.itemCount.toString(), style: TextStyle(color: Theme.of(context).primaryColor !=  Colors.black ? Colors.black : Colors.white),),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
    activeIcon: Consumer<Cart>(builder: (_, cart, child) => 
      Container(
        height: 40,
        width: 70,
        decoration: BoxDecoration(
        border: Border.all(
          color: Colors.transparent, width: 4),
          color: Colors.transparent,
          shape: BoxShape.circle),
        child: Stack(
          overflow: Overflow.visible,
          children: [
            Positioned(
              bottom: 5,
              left: 2,
              child: FloatingActionButton(
                backgroundColor: Theme.of(context).primaryColor,
                onPressed: () {
                  Navigator.pushReplacementNamed(context, navigationBarRoutes[1]);
                },
                child: Icon(
                  Icons.shopping_cart,
                  color: Theme.of(context).primaryColor !=  Colors.black ? Colors.black : Colors.white
                ),
                elevation: 5,
              ),
            ),
            Positioned(
              bottom: 34,
              left: 37,
              child: Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white, width: 4),
                    color: Theme.of(context).primaryColor,
                    shape: BoxShape.circle),
                child: Consumer<Cart>(builder: (_, cart, child) => 
                  Center(
                    child: Text(cart.itemCount.toString(), style: TextStyle(color: Theme.of(context).primaryColor !=  Colors.black ? Colors.black : Colors.white),),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
    title:Container(
      height: 0.0,
    )
  ),
  BottomNavigationBarItem(
    icon: Icon(
      Icons.person,
      color: Colors.grey,
      size: 35,
    ),
    activeIcon:Icon(
      Icons.person,
      color: Theme.of(context).primaryColor,
      size: 35,
    ),
    title:Container(
      height: 0.0,
    )
  )
];

BottomNavigationBar generateBottomNavigationBar(context, currentIndex) {
  return BottomNavigationBar(
    items: tabItems(context),
    onTap: (index) {
      if (index != currentIndex) {
        Navigator.pushReplacementNamed(context, navigationBarRoutes[index]);
      }
    },
    type: BottomNavigationBarType.fixed,
    backgroundColor: Colors.white,
    selectedItemColor: Color(0xFF3D8DFB),
    currentIndex: currentIndex,
    selectedFontSize: 12.0,
  );
}
