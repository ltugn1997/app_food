import 'package:app_food/units/constant.dart';
import 'package:flutter/material.dart';

class SearchWidget extends StatefulWidget {
  SearchWidget({Key key}) : super(key: key);

  @override
  _SearchWidgetState createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  @override
  Widget build(BuildContext context) {
    return  Material(
      elevation: 14.0,
      shadowColor: Colors.blueAccent,
      borderRadius: BorderRadius.circular(30),
      child: TextFormField(
        onChanged: (String text) {},
        autofocus: false,
        decoration: InputDecoration(
            focusColor: Colors.blueAccent,
            contentPadding: EdgeInsets.symmetric(vertical:0),
            prefixIcon: Icon(
              Icons.search,
              color: Colors.grey,
            ),
            hintText: "Search",
            filled: true,
            fillColor: Colors.white,
            border: Contants.border,
            focusedBorder: Contants.border,
            ),
      ),
    );
  }
}