import 'package:flutter/material.dart';

class WidgetCost extends StatelessWidget {
  final double cost;
  WidgetCost({Key key, this.cost}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return RichText(
          text: TextSpan(children: [
          TextSpan(
            text: '\$',
            style: TextStyle(
                fontSize: 22,
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold),
          ),
          TextSpan(
            text: cost.toStringAsFixed(2),
            style: TextStyle(
              fontSize: 24,
              color: Colors.black,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}