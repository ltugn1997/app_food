import 'package:app_food/Providers/cart.dart';
import 'package:app_food/widgets/button_caculator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app_food/widgets/widget_cost.dart';
class MyCartCard extends StatefulWidget {
  final String imgURl ;
  final double cost;
  final String name;
  final double entrance;
  final int quantity;
  final String id;
  MyCartCard({Key key, this.imgURl, this.cost, this.name, this.entrance, this.quantity, this.id}) : super(key: key);

  @override
  _MyOrderCardState createState() => _MyOrderCardState();
}

class _MyOrderCardState extends State<MyCartCard> {
  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<Cart>(context);
    Size size = MediaQuery.of(context).size;

    int quantityMain = widget.quantity;

    void _showCupertinoDialog() {
      showDialog(
        context: context,
        builder: (_) => new CupertinoAlertDialog(
          title: new Text("Remove Item"),
          content: new Text("Are You Sure ?"),
          actions: <Widget>[
            Container(
              child: FlatButton(
                child: Text('Cancel'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ),
            Container(
              child: FlatButton(
                child: Text('Confirm'),
                onPressed: () {
                  cart.removeItem(widget.id);
                  Navigator.of(context).pop();
                },
              )
            )
          ],
        )
      );
    }

    void funtionAddQuantity(){
      setState(() {
        setState(() {
          quantityMain +=1;
          cart.addItem(widget.id, quantity: quantityMain, typeCart: true);
        });
      });
    }
    void funtionRemoveQuantity(){
      setState(() {
          if(quantityMain == 1){
            _showCupertinoDialog();
          }
          if (quantityMain > 1)
            setState(() {
              quantityMain -=1;
              cart.addItem(widget.id, quantity: quantityMain, typeCart: true);
          });
      });
    }
    return Card(
      margin: EdgeInsets.only(top: 5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FittedBox(
            child: 
              Container(
                margin: EdgeInsets.only(left: 20 ),
                child: Row(children: [             
                  Container(
                    height: size.height * 0.18,
                    width: size.width * 0.33,
                    child: Image.asset(widget.imgURl, fit: BoxFit.contain,),
                  ),                   
                  SizedBox(width: 15,),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      WidgetCost(cost:widget.cost),
                      SizedBox(height: 5,),
                      DefaultTextStyle(style: Theme.of(context).textTheme.headline6, child: Text(widget.name, style: TextStyle(fontWeight: FontWeight.bold),),),
                      DefaultTextStyle(style: Theme.of(context).textTheme.bodyText1, child:Text('Entrance ' + widget.entrance.toString() + ' g', style: TextStyle(color: Colors.grey[600]),),),
                      Container(
                        margin: EdgeInsets.only(top: 10, right: 20),
                        child: ButtonCaculator(quantity: widget.quantity, funtionAddQuantity: funtionAddQuantity, funtionRemoveQuantity: funtionRemoveQuantity,),
                      ),
                    ],
                  ),              
                ],
              ),
            ),
          ),
      ],)
    );
  }
}