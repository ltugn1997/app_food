import 'package:app_food/Providers/food_type.dart';
import 'package:app_food/units/constant.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Categories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Foodtypes foodtypes = Provider.of<Foodtypes>(context, listen: false);
    final dishs = foodtypes.items; 
    Size size = MediaQuery.of(context).size;
    return Container(
        margin: EdgeInsets.only(top: 10),
        height: size.height * 0.1,
        width: MediaQuery.of(context).size.width,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (ctx, i) {
            Foodtype dataFoodItem = dishs[i];
            return Container(
              padding: EdgeInsets.all(10),
              child: InkWell(
                onTap: (){Navigator.of(context).pushNamed('/italian_item', arguments: dataFoodItem);},
                child: Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Container(
                    width: size.width * 0.29,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Theme.of(context).primaryColor,
                          spreadRadius: 2,
                          blurRadius: 2,
                          offset: Offset(0, 0),
                        )
                      ],
                      color: Colors.white,
                      border: Border.all(
                        color: Theme.of(context).primaryColor,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Center(
                        child: Row(
                          children: [
                            Hero(
                            tag: dataFoodItem.profileImage +'type',
                              child: Text(
                                dataFoodItem.image ,
                                style: TextStyle(fontSize: 20),
                              ),
                            ),
                            SizedBox(width: 5,),
                            Expanded(
                              child: Container(
                                child: Text(
                                  dataFoodItem.title, 
                                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                                  ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                ),
              );
            },
          itemCount: Contants.carts.length,
        ),
    );
  }
}