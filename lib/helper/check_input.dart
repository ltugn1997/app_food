bool validateEmail(dynamic value) {
  const Pattern pattern = ValidatePatern.email;
  final RegExp regex = RegExp(pattern);
  if (!regex.hasMatch(value)) {
    return false;
  }
  return true;
}
bool validatePassword(dynamic value) {
  const Pattern pattern = ValidatePatern.password;
  final RegExp regex = RegExp(pattern);
  if (!regex.hasMatch(value)) {
    return false; 
  }
  return true;
}
class ValidatePatern{
  static const Pattern email = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  static const Pattern password = r'^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{7,64})$';
}
