import 'package:app_food/Providers/auth.dart';
import 'package:app_food/Providers/cart.dart';
import 'package:app_food/Providers/food_type.dart';
import 'package:app_food/Providers/order.dart';
import 'package:app_food/screens/connect_screen.dart';
import 'package:app_food/screens/detail_item.dart';
import 'package:app_food/screens/italian_food.dart';
import 'package:app_food/screens/login_screen.dart';
import 'package:app_food/screens/my_cart.dart';
import 'package:app_food/screens/orders_screen.dart';
import 'package:app_food/screens/setting_screen.dart';
import 'package:app_food/screens/subItem.dart';
import 'package:flutter/material.dart';
import 'package:app_food/screens/home_page.dart';
import 'package:app_food/Providers/food_items.dart';
import 'package:provider/provider.dart';
import 'package:app_food/screens/profile_screen.dart';

import 'Providers/setting.dart';
void main() {
  runApp(  MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => SettingModel(),),
        ChangeNotifierProvider(
          create: (ctx) => AuthUsers(),),
        ChangeNotifierProvider(
          create: (ctx) => Foods(),),
        ChangeNotifierProvider(
          create: (ctx) => Cart(),),
        ChangeNotifierProvider(
          create: (ctx) => Foodtypes(),),
        ChangeNotifierProvider(
          create: (ctx) => Orders(),),
      ],
      child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    const Color yellow = Color(0xFFF0C143);
    SettingModel settingModel = Provider.of<SettingModel>(context, listen: true);
    return
  MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: settingModel.appColor ?? yellow,
        ),
        home: HomePage(),
        routes: {
          '/profile_screen': (context) => ProfileScreen(),
          '/detail_item': (context) => DetailItem(),
          '/italian_item': (context) => ItalianFood(),
          '/my_cart': (context) => MyCart(),
          '/screen_order':(context) => OrdersScreen(),
          '/sub_item':(context) => SubItem(),
          '/login_screen':(context) => LoginScreen(),
          '/setting_screen' :(context) => SettingScreen(),
          '/connect_screen' :(context) => ConnectScreen(),
        },
    );
  }
}