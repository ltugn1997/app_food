import 'package:app_food/models/food_item.dart';
import 'package:flutter/material.dart';

class CartItem{
  final String id;
  final double price;
  final int quantity;
  final Food food;

  CartItem({
    @required this.id,
    @required this.price,
    @required this.quantity,
    @required this.food,
  });
}

class Cart with ChangeNotifier{
  Map<String, CartItem> _items = {};

  Map<String, CartItem> get items{
    return{..._items};
  }

  int get itemCount{
    int count = 0;
     _items.forEach((key, cartItem) {
      count += cartItem.quantity;
    });
    return count;
  }

  double get totalAmount{
    var total = 0.0;
    _items.forEach((key, cartItem) {
      total += cartItem.price * cartItem.quantity;
    });
    return total;
  }

  void addItem(String foodId, {double price, Food food, int quantity = 1, bool typeCart = false}){
    if(_items.containsKey(foodId)){
      _items.update(foodId,(existingCartItem) {
      int number;
      if (typeCart){
        number = 0;
      }
      if (typeCart == false){
        number = existingCartItem?.quantity ?? 0;
      }
      return
      CartItem(id: existingCartItem.id,
        price: existingCartItem.price,
        quantity: number + quantity,
        food: existingCartItem.food,
      );}
    );
    }
    if(!_items.containsKey(foodId)){
      _items.putIfAbsent(foodId, () => CartItem(id:DateTime.now().toString(), price: price, quantity: quantity, food: food));
    }
    notifyListeners();
  }

  void removeItem(String productId){
    _items.remove(productId);
    notifyListeners();
  }

  void clear() {
    _items = {};
    notifyListeners();
  }
}