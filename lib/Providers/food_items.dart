import 'package:app_food/units/constant.dart';
import 'package:flutter/material.dart';

import 'package:app_food/models/food_item.dart';

class Foods with ChangeNotifier{
  List<Food> _items = Contants.dishItems;

  List<Food> get items{
    return [... _items];
  }

  void addFood(){
  
    notifyListeners();
  }
}