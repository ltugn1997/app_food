import 'package:flutter/material.dart';
import 'package:app_food/units/constant.dart';

class AuthUser{
  final String userName;
  final String gmail;
  final String passWord;
  final String phone;
  final String id;

  AuthUser({
    @required this.userName,
    @required this.gmail,
    @required this.passWord,
    @required this.phone,
    this.id,
  });
}

class AuthUsers with ChangeNotifier{
  List<AuthUser> _items = Contants.users;
  AuthUser selectedUser = Contants.users[0];

  List<AuthUser> get items{
    return [... _items];
  }

  void addUser(String gmail, String passWord, String userName, String phone) {
    _items.insert(
      0,
      AuthUser(userName: userName, passWord: passWord, gmail: gmail, phone: phone, id: DateTime.now().toString())
    );
    notifyListeners();
  }

  void getCurrentUser(String id){
    List<AuthUser> currentUserList = items.where((AuthUser authUser) => authUser.id == id).toList();
    if (currentUserList.isEmpty){
      selectedUser = Contants.users[0];
      notifyListeners();
    }
    if (currentUserList.isNotEmpty){
      selectedUser = currentUserList[0];
      notifyListeners();
    }
  }

  void editUser(String id, String gmail, String passWord, String userName, String phone){
    AuthUser oldSelectedUser = _items.firstWhere((AuthUser authUser) => authUser.id == id);
    AuthUser newSelectedUser  = AuthUser(
      id: oldSelectedUser.id,
      gmail: gmail ?? oldSelectedUser.gmail,
      passWord: passWord ?? oldSelectedUser.passWord,
      userName: userName ?? oldSelectedUser.userName,
      phone: phone ?? oldSelectedUser.phone,
    );

    _items.remove(oldSelectedUser);
    _items.add(newSelectedUser);
    selectedUser = newSelectedUser;

    notifyListeners();
  }
}