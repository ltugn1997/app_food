import 'package:flutter/material.dart';

class SettingModel with ChangeNotifier{
  Color appColor;
  Color tempAppColor;

  SettingModel({
    @required this.appColor,
    @required this.tempAppColor,
  });


  void changeColor(Color color) {
    appColor = color;
    notifyListeners();
  }

  void changeTempColor(Color color) {
    tempAppColor = color;
    notifyListeners();
  }
}
