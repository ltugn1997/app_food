import 'package:app_food/models/food_item.dart';
import 'package:flutter/material.dart';
import 'package:app_food/units/constant.dart';

class Foodtype{
  final String image;
  final String title;
  final Color color;
  final Color colorText;
  final String profileImage;
  final List<Food> foods;
  final List type;
  

  Foodtype({
    @required this.image,
    @required this.title,
    @required this.color,
    @required this.colorText,
    @required this.profileImage,
    @required this.foods,
    @required this.type,
  });
}

class Foodtypes with ChangeNotifier{
  List<Foodtype> _items = Contants.carts;

  List<Foodtype> get items{
    return [... _items];
  }
}