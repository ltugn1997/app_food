three main screen default when user haven't orderd yet

![Screen_Recording_2021-03-21_at_15.34.10](/uploads/796cbd6253f6b655a4598b43eb98f2e7/Screen_Recording_2021-03-21_at_15.34.10.gif)

introduction main screen

![home](/uploads/58c138ee398897490a92d9c9a7225f62/home.gif)

Detail Screen you can see more information of food you selected before you buy

![search](/uploads/d33b7206d8a12c52ef5301a4b99a8769/search.gif)

Search screen: the screen help you find foot you want

![Screen_Recording_2021-03-21_at_16.51.56](/uploads/702322cf03164b27358271f36b124f9e/Screen_Recording_2021-03-21_at_16.51.56.gif)

One of the type screen and food screen

![Screen_Recording_2021-03-21_at_16.50.18](/uploads/33ffe5cba5bd99ded944eebc863bdf79/Screen_Recording_2021-03-21_at_16.50.18.gif)

Cart screen you can check last times, surfing to the left to remove product, increase or subtract the number of product, press button confirm then you have a order

![cart](/uploads/bf5cb07b8755d7521ec237ba43ad4c8b/cart.gif)

your orders screen: The screen help you see your history purchase

![Screen_Recording_2021-03-21_at_16.52.19](/uploads/a3ef1d601eafbcddae78469a5e4d38bf/Screen_Recording_2021-03-21_at_16.52.19.gif)

Login with the new account

![Screen_Recording_2021-03-21_at_16.53.59](/uploads/1e3de1f33c29c85acc19680b85d02d4b/Screen_Recording_2021-03-21_at_16.53.59.gif)

you can edit information for user

![Screen_Recording_2021-03-21_at_16.52.55](/uploads/12da19a8c684e2af57e1403c3f817003/Screen_Recording_2021-03-21_at_16.52.55.gif)
